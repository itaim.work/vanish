using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class GameManager  : MonoBehaviour
{
    [SerializeField] private Color[] ButtonColors;
    [SerializeField] private GameObject[] Buttons;

    // Start is called before the first frame update
    void Start()
    {
        GameObject temp;
        Buttons = GameObject.FindGameObjectsWithTag("Button");
        ButtonColors = new Color[4];
        ButtonColors[0] = Color.red;
        ButtonColors[1] = Color.yellow;
        ButtonColors[2] = Color.green;
        ButtonColors[3] = Color.blue;


        

        for (int i = 0; i < ButtonColors.Length; i++)
        {
            temp = Buttons[UnityEngine.Random.Range(0, Buttons.Length)];
            while (temp.GetComponent<Button_Controller>().GetBeenShuffeld())
            {
                temp = Buttons[UnityEngine.Random.Range(0, Buttons.Length)];
            }
            temp.GetComponent<Button_Controller>().SetBeenShuffeld(true);
            temp.GetComponent<Button_Controller>().SetColor(ButtonColors[i]);
            temp.GetComponent<Button_Controller>().SetButtonNumber(i + 1);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }

        print(DataHolder.MouseSens);
    }

    public bool ActiveCamera(Transform Base ,Transform target,  float maxRadius, float maxAngle)
    {
        Vector3 DirectonBetween = (target.position - Base.position).normalized; // red line direction
        float angle;
        RaycastHit hit;
        bool isInFOV = false;
        if (Physics.Raycast(Base.position + Vector3.zero, DirectonBetween, out hit, maxRadius))
        {
            if (LayerMask.LayerToName(hit.transform.gameObject.layer).Equals("Player"))
            {
                angle = Vector3.Angle(Base.forward + Vector3.zero, DirectonBetween);
                if (angle <= maxAngle)
                {
                    isInFOV = true;
                }
                else
                {
                    isInFOV = false;

                }
            }
            else
            {
                isInFOV = false;

            }
        }

        return isInFOV;
    }
}
