using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private CharacterController Controller;

    Vector3 move;
   public float speed = 5, x, z;


    // Start is called before the first frame update
    void Start()
    {
        Controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        x = Input.GetAxis("Horizontal");
        z = Input.GetAxis("Vertical");

        move = transform.right * x + transform.forward * z;
        move = move * speed * Time.deltaTime;

        Controller.Move(move);



    }

    public void TakeDamage(int damage)
    {
        //health-=damage;
        //healthBar.Sethealth()
    }

}
