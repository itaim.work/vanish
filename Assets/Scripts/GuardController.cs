using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class GuardController : MonoBehaviour
{
    [SerializeField] private bool IsInFOV;

    [SerializeField] private Transform target;
    [SerializeField] private NavMeshAgent Agent;

    [SerializeField] private GameManager GameManagerScript;
    [SerializeField] private GameObject[] WayPoints;
    private int counter = 0;
    public float timer, timerReset = 5;
    [SerializeField] private float maxRadius;
    [SerializeField] private float maxAngle;


    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.Find("Player").GetComponent<Transform>();
        Agent = gameObject.GetComponent<NavMeshAgent>();
        GameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();

        WayPoints = GameObject.FindGameObjectsWithTag("WayPoint");

        Array.Sort(WayPoints, CompareByName);

        Agent.SetDestination(WayPoints[counter++].transform.position);

        timer = timerReset;
    }

    // Update is called once per frame
    void Update()
    {
        IsInFOV = GameManagerScript.ActiveCamera(transform, target, maxRadius, maxAngle);
        if (Agent.remainingDistance <= 0.5f && !IsInFOV)
        {
            Agent.SetDestination(WayPoints[counter++].transform.position);
        }
        if (counter >= WayPoints.Length)
        {
            counter = 0;
        }
        if (IsInFOV)
        {
            Agent.SetDestination(target.position);
        }


        if (timer <= 0)
        {
            timer = timerReset;
        }

        timer = timer - Time.deltaTime;

    }

    public int CompareByName(GameObject a, GameObject b)
    {
        return a.transform.name.CompareTo(b.transform.name);
    }

    /// <summary>
    /// visualize the FOV process
    /// </summary>
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;//A color for the Radius
        Gizmos.DrawWireSphere(transform.position, maxRadius);//Draw the radius with a yellow color 

        Vector3 fovLine1 = Quaternion.AngleAxis(maxAngle, transform.up) * transform.forward * maxRadius;//sets the fov aspect, with the radius and the look angel(marked with !)
        Vector3 fovLine2 = Quaternion.AngleAxis(-maxAngle, transform.up) * transform.forward * maxRadius;//!
        Vector3 fovLine3 = Quaternion.AngleAxis(-maxAngle, transform.right) * transform.forward * maxRadius;//!
        Vector3 fovLine4 = Quaternion.AngleAxis(maxAngle, transform.right) * transform.forward * maxRadius;//!


        Gizmos.color = Color.blue;//sets the FOV lines coloer to blue
        Gizmos.DrawRay(transform.position, fovLine1);//draws the FOV aspect on the screen @
        Gizmos.DrawRay(transform.position, fovLine2);//@
        Gizmos.DrawRay(transform.position, fovLine3);//@
        Gizmos.DrawRay(transform.position, fovLine4);//@



        if (!IsInFOV)
            Gizmos.color = Color.red;
        if (IsInFOV)
            Gizmos.color = Color.green;//the player position ray color
        Gizmos.DrawRay(transform.position, (target.transform.position - transform.position).normalized * maxRadius);//draws a line to the player position

        Gizmos.color = Color.black;//the forward ray color
        Gizmos.DrawRay(transform.position, transform.forward * maxRadius);//draws a ray to the AI forward direction
    }

}
