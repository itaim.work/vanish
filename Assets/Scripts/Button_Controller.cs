using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button_Controller : MonoBehaviour
{
    [SerializeField] private float maxDist, dist;
    [SerializeField] private int ButtonNumber;
    [SerializeField] private bool BeenShuffeld, ButtonPressed;
    [SerializeField] private Transform Player;
    [SerializeField] private UI_Controller UI_Controller_Script;
    [SerializeField] private GameObject Button_Display_Text;


    // Start is called before the first frame update
    void Start()
    {
        BeenShuffeld = false;
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        dist = Vector3.Distance(transform.position, Player.position);

        if (dist <= maxDist)
        {
            if (Physics.Raycast(Player.position, Camera.main.transform.forward, out hit, maxDist))
            {
                if (hit.transform.CompareTag("Button"))
                {
                    ButtonPressed = false;
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        print("Open door");
                    }
                }
                else
                {
                    ButtonPressed = true;
                }
            }
            else
            {
                ButtonPressed = true;

            }
        }
        else
        {
            ButtonPressed = true;
        }
        if (ButtonPressed)
        {
            Button_Display_Text.SetActive(false);
        }
        else
        {
            Button_Display_Text.SetActive(true);
        }

    }

    public void SetButtonNumber(int value)
    {
        this.ButtonNumber = value;
    }
    public int GetButtonNumber()
    {
        return this.ButtonNumber;
    }
    public void SetBeenShuffeld(bool value)
    {
        this.BeenShuffeld = value;
    }
    public bool GetBeenShuffeld()
    {
        return this.BeenShuffeld;
    }
    public void SetColor(Color value)
    {
        gameObject.GetComponent<Renderer>().material.color = value;
    }
}
