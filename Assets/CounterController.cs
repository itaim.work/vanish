using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CounterController : MonoBehaviour
{
    [SerializeField] private float counter, displayCounter;
    [SerializeField] private Text CounterDisplayText;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        counter -= Time.deltaTime;
        displayCounter = (int)counter;
        if (counter <= 0)
        {
            print("Counter done");
        }
        CounterDisplayText.text = "Time Left: " + (int)displayCounter / 60 + " : " + displayCounter % 60;
    }
}
